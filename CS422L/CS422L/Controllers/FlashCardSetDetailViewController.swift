//
//  FlashcardSetDetailViewController.swift
//  CS422L
//
//  Created by Luke Norris on 2/10/21.
//

import Foundation
import UIKit

class FlashcardSetDetailViewController: UIViewController, UITableViewDelegate,
                                        UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cardCell", for: indexPath) as! FlashcardTableViewCell
        //setup cell display here
        cell.cardLabel.text = cards[indexPath.row].term
        return cell
    }
    
    var cards: [Flashcard] = [Flashcard]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        
        
        /*ran into this bug while trying to finish up xcodes portion, any clue to how my optional wrap could have messed this up?
            ~Luke
        */
        
        self.tableView.dataSource = self
        let cardClass = Flashcard()
        //connect hard coded collection to sets
        cards = cardClass.getHardCodedCollection()
    
    
    }
    @IBAction func addSet() {
        let card = Flashcard()
        card.term = "it works"
        card.definition = ":)"
        cards.append(card)
        tableView.reloadData()
    }
}
