//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    @IBOutlet weak var collectionView: UICollectionView!
    var sets: [FlashcardSet] = [FlashcardSet]()


    override func viewDidLoad() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        super.viewDidLoad()
        let setClass = FlashcardSet()
        //connect hard coded collection to sets
        sets = setClass.getHardCodedCollection()

    }
    
    @IBAction func addCard() {
        let set = FlashcardSet()
        set.title = "new"
        sets.append(set)
        collectionView.reloadData()
    }
    
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return number of items
        return sets.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        //setup cell display here
        cell.textLabel.text = sets[indexPath.row].title
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        performSegue(withIdentifier: "GoToDetail", sender: self)

        
        
    }


}
